#include <cstdio>
#include <initializer_list>
#include <omp.h>


//#define FMT_HEADER_ONLY
//#include "format.h"

using namespace std;
//using namespace fmt;

int main()
{
	int ntprocs;
	#pragma omp target map(from:ntprocs)
	ntprocs = omp_get_num_procs();
	int ncases=40, nteams=4, chunk=ntprocs;
	#pragma omp target
	#pragma omp teams num_teams(nteams) thread_limit(ntprocs/nteams)
	#pragma omp distribute
	for (int starti=0; starti<ncases; starti+=chunk)
		#pragma omp parallel for
		for (int i=starti; i<starti+chunk; i++)
		{
			printf(	"case i=%d/%d by team=%d/%d thread=%d/%d\n",
					i+1,					ncases,
					omp_get_team_num()+1,	omp_get_num_teams(),
					omp_get_thread_num()+1,	omp_get_num_threads() );
			
			//fmt::print( "hallo {}\n", omp_get_thread_num()+1 );
		}
}

/*
== Resultate ==

ohne OpenMP:
real	0m0.029s
user	0m0.028s

mit OpenMP:
real	0m0.013s
user	0m0.040s

*/

