#include <iostream>
using namespace std;

int main()
{
	long long int sum = 0;

	#pragma omp parallel
	#pragma omp for reduction(+:sum)
	for( int i = 0; i < 10000000; i++ )
		sum += i;

	cout << sum << endl;
	return sum - 49999995000000;
}

/*
== Resultate ==

ohne OpenMP:
real	0m0.029s
user	0m0.028s

mit OpenMP:
real	0m0.013s
user	0m0.040s

*/

