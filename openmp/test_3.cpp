#include <cstdio>
#include <iostream>
#include <omp.h>

#include <vector>

//#define FMT_HEADER_ONLY
//#include "format.h"

using namespace std;
//using namespace fmt;

//const auto N = 262144*16;
//const auto N = 262144;
const auto N = 1024;

int main()
{
	{
		cout << N*N << endl;
		vector<double>	a( N*N ),
						b( N*N ),
						c( N*N );

		for( long long int i=0; i < N*N; ++i )
			b[i] = a[i] = (double)i;
		
		#pragma omp parallel for simd
		for( int i=0; i<N; i++ )
		{
			for( int j=0; j<N; j++ )
			{
				double temp = 0.0;
				for( int k=0; k<N; k++ )
				{
					temp += a[i + k*N]
						  * b[k + j*N];
				}
				c[i + j*N] = temp;
			}
		}
		
		long double sum = 0;
/*		for( int i=0; i<N; i++ )
		{
			for( int j=0; j<N; j++ )
			{
				sum += c[i + j*N];
			}
		}*/
		
		printf(	"sum = %.2lf\n", (double)sum );
	}
	// double a[N], b[N]; // existieren auf dem Stack, Crash bei Größe >262144
	vector<double>	a( N ),
					b( N );
	long long		d1 = 0;
	double			d2 = 0.0;

	#pragma omp simd reduction(+:d1)
	for( int i=0; i<N; i++ )
		d1 += i*(N+1-i);
		
	#pragma omp simd
	for( int i=0; i<N; i++ )
	{
		a[i] = i;
		b[i] = N+1-i;
	}

	// Macht das Programm langsammer weil sich der overhead der Threaderzeugung nicht lohnt.
	// Es verändert ausserdem das Resultat, durch eine andere Ausführungsreihenfolge der Floating Point Operationen
	// präzise: 12297838178563260416.00
	// aktiv  : 12297838178563258368.00 abweichung: 1.66e-16
	// inaktiv: 12297838178563282944.00 abweichung: 1.83e-15
	#pragma omp parallel for simd reduction(+:d2)
	for( int i=0; i<N; i++ )
		d2 += a[i]*b[i];

	printf(	"result1 = %lld\n"
			"result2 = %.2lf\n", d1, d2 );
}

/*
== Resultate ==

ohne alle OpenMP:
real	0m0.006s
user	0m0.008s
(wirklich schneller)

mit allen OpenMP:
real	0m0.014s
user	0m0.024s

*/

