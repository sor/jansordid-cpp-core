#ifndef SOR_CORE_H
#define SOR_CORE_H

#include <iostream>

namespace JanSordid
{
	using namespace std;

	#define nullptr __null

	#define countof( a ) (sizeof(a) / sizeof((a)[0]))

	#define n_times_i( n, i )	for( int i = 0; i < (n); i++ )
	#define foreach( a, i )		n_times_i( countof(a), i )

	#define SWAP( x, y ) do\
	{	unsigned char swap_temp[sizeof(x) == sizeof(y) ? (signed)sizeof(x) : -1];\
		memcpy( swap_temp, &(y),	sizeof(x) );\
		memcpy( &(y), &(x),			sizeof(x) );\
		memcpy( &(x), swap_temp,	sizeof(x) );\
	} while( 0 )

	#define MAX( a, b )		((a) < (b) ? (b) : (a))
	#define MIN( a, b )		((a) < (b) ? (a) : (b))
	#define MED( a, b, c )	((a) < (b)\
								? ((b) < (c) ? (b) : MAX( a, c ))\
								: ((a) < (c) ? (a) : MAX( b, c )))
	#define MAXP( a, b )	((*a) < (*b) ? (b) : (a))
	#define MINP( a, b )	((*a) < (*b) ? (a) : (b))
	#define MEDP( a, b, c )	((*a) < (*b)\
								? ((*b) < (*c) ? (b) : MAXP( a, c ))\
								: ((*a) < (*c) ? (a) : MAXP( b, c )))

	class Core
	{
	private:
	public:
	};
}

#endif
