#include <cstdlib>

#include "qsort.h"

using namespace JanSordid;

int main()
{
	int ary[] = { 9, 9, 9, 13, 42, 9, 1 };
//	int ary[] = { 5, 6, 9, 13, 42, 9, 11 };

	foreach( ary, i )
		cout << ary[i] << " ";
	cout << endl;

	QSort<int*> qs( ary, ary + countof(ary) );

	qs();

	foreach( ary, i )
		cout << ary[i] << " ";
	cout << endl;

	return EXIT_SUCCESS;
}
